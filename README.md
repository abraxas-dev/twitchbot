twitchbot
===================
IRC bot to display Twitch streaming information

#Deprecated
**twitchbot** has now merged into a submodule of the [CasulBot IRC Python Engine](https://bitbucket.org/mitch-b/casul-ircbot-python). 





##Dependencies
**twitchbot** uses the [CasulBot IRC Python Engine](https://bitbucket.org/mitch-b/casul-ircbot-python) for connections/communication. This engine has been included as a submodule to this repository.

##Getting Started
In order to get up and running, you'll need [Python 2.7](https://www.python.org/download/releases/2.7.7/). In your terminal, navigate to where you want to clone this project. Run the following commands to pull all required files:

```bash
$> git clone https://bitbucket.org/abraxas-dev/twitchbot.git
$> cd twitchbot
$> git submodule update --init --recursive
$> pip install -r requirements.txt
```

If you do not have `pip` installed, you will probably want to do so. It's fantastic. [pip Installation Guide](https://pip.pypa.io/en/latest/installing.html).

This will clone the repository, and pull latest of the submodule (casul IRC Python Engine) with it. This will allow you to make any changes to the engine (that you don't want back in actual engine itself), while still being able to pull latest updates to code base (if wanted).

##Configuring

You'll want to create a new file named `config.ini` in the root directory of newsbot-lite.
The contents of the file should look like this, simply modify the values as needed:

```bash
[casulirc]
About: Twitch Bot
	> mitch-b
	> https://bitbucket.org/abraxas-dev/twitchbot
	0.0.1, 06/09/2014
Verbose: True
Host: irc.server.com
Port: 6667
Channel: #channel
Password:
Nick: twitchbot
User: twitchbot
HostName: server.com
ServerName: server
RealName: TwitchBot

[TwitchAPI]
Version: v2
Stream: user
ClientID: --YOUR_CLIENT_ID--
ClientSecret: --YOUR_CLIENT_SECRET_ID--
RedirectURL: http://server.com/oauth_authorization
Endpoint: https://api.twitch.tv/kraken

[Monitor]
Interval:120
```

This file is found in the .gitignore, so you won't need to worry about accidentally commiting any sensitive information.

##Testing

Simply run:

```bash
$> python main.py
```

and the casul IRC Engine will attempt to connect and listen to the specified channel. This bot will run at interval set in `config.ini` to check Twitch API Stream status. If streaming, it will update the user's status accordingly. When finished streaming, it will update IRC channel topic to inform users who unfortunately missed the event.

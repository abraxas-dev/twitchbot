#!/usr/bin/env python

# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

"""Twitch Bot

Auto-post when configured user is streaming, and when finished.
"""

from engine.casulirc import engine
from engine.casulirc import settings

from threading import Thread
import ConfigParser
import requests
import json
import time

class TwitchBot:
  def __init__(self):
    global bot
    global twitchSettings

    twitchSettings = settings.Settings()

    config = ConfigParser.ConfigParser()
    config.read('./config.ini')
    twitchSettings.about = config.get('casulirc', 'About')
    twitchSettings.host = config.get('casulirc', 'Host')
    twitchSettings.channel = config.get('casulirc', 'Channel')
    twitchSettings.password = config.get('casulirc', 'Password')
    twitchSettings.nick = config.get('casulirc', 'Nick')
    twitchSettings.verbose = config.get('casulirc', 'Verbose')
    twitchSettings.user = config.get('casulirc', 'User')
    twitchSettings.hostname = config.get('casulirc', 'HostName')
    twitchSettings.server = config.get('casulirc', 'ServerName')
    twitchSettings.realname = config.get('casulirc', 'RealName')

    # twitch-bot specific settings
    twitchSettings.ClientID = config.get('TwitchAPI', 'ClientID')
    twitchSettings.ClientSecret = config.get('TwitchAPI', 'ClientSecret')
    twitchSettings.RedirectURL = config.get('TwitchAPI', 'RedirectURL')
    twitchSettings.Endpoint = config.get('TwitchAPI', 'Endpoint')
    twitchSettings.Stream = config.get('TwitchAPI', 'Stream')
    twitchSettings.Version = config.get('TwitchAPI', 'Version')

    # monitoring settings
    twitchSettings.Interval = config.get('Monitor', 'Interval')

    bot = engine.CasulIRC(callback=self.callback,
                          settings=twitchSettings)

    monitorThread = Thread(target = self.monitor)
    monitorThread.start()
    print ''
    bot.run()



  def twitch_headers(self):
    return {'Client-ID': twitchSettings.ClientID,
            'content-type': 'application/vnd.twitchtv.{0}+json'.format(twitchSettings.Version)}

  def monitor(self):
    streaming = False
    while True:
      url = '{0}/streams/{1}'.format(twitchSettings.Endpoint, twitchSettings.Stream)
      try:
        result = requests.get(url,headers=self.twitch_headers())
        if result.status_code == 200:
          data = json.loads(result.text)
          if data is None:
            raise Exception('Failure to read JSON result from Twitch API')
          stream = data['stream']
          if stream and streaming == False:
            playing = stream[u'game']
            url = stream[u'channel'][u'url']
            bot.TOPIC("Abraxas is now streaming '{0}' : {1}".format(playing, url))
            streaming = True
          elif stream is None and streaming == True:
            bot.TOPIC("Stream offline for now! <3")
            streaming = False
        else:
          print 'Unable to read from {0}'.format(url)
      except Exception as e:
        streaming = False
        print e
      time.sleep(float(twitchSettings.Interval))


  def callback(self, sender, message):
    bot.PRIVMSG("Plz be quiet. I'm working hard on other things right now...")

if __name__ == '__main__':
  TwitchBot()
